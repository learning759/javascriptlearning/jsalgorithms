// problemStatement: Give a number 'n', find the factorial of that integer
// eg: factorial(4) = 4*3*2*1 = 24

// ...................................................................................

// Solution

const factorial = (n) => {
    result = 1
    for (let i = 2; i <= n; i++) {
        result *= i
    }
    return result
}

console.log(factorial(5)) // 120
console.log(factorial(1)) // 1
console.log(factorial(0)) // 1
console.log(factorial(4)) // 24


// Big O -O(n)