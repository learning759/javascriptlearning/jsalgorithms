// ProblemStatement: Given a number 'n', find the first 'n' elements of the Fibonacci sequence

// In mathematics Fibonacci seq is a sequence in which each number is the sum of the two preceeding ones.
// The first Two numbers in the seq are 0 and 1.

// ..........................................................................................................

// Solution 

const fibonacci = (n) => {
    const fib = [0, 1]
    for (let i = 2; i < n; i++) {
        fib[i] = fib[i - 1] + fib[i - 2]
    }
    return fib;
}

console.log(fibonacci(3)); //[0,1,1]
console.log(fibonacci(2)); //[0,1]
console.log(fibonacci(7)); //[0,1,1,2,3,5,8]

// Big O - O(n) if n increases for loop execution time also incereases